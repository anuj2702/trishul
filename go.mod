module trishul

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/emicklei/proto v1.6.7
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mgechev/revive v0.0.0-20181210140514-b4cc152955fb // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/olekukonko/tablewriter v0.0.1 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.0
	golang.org/x/sys v0.0.0-20181213200352-4d1cda033e06 // indirect
	golang.org/x/tools v0.0.0-20181214171254-3c39ce7b6105 // indirect
	gopkg.in/src-d/go-git.v4 v4.8.1
)
