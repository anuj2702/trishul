package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

//Version stores the version of trishul
const Version = `v1.0.1`

// versionCmd represents the setup command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Shows the version of trishul",
	Long:  `trishul version`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(Version)
	},
	Args: cobra.MaximumNArgs(0),
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
