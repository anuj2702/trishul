package cmd

import (
	"trishul/cmd/install/git"
	"trishul/cmd/install/golang"
	"trishul/cmd/install/manager"
	"trishul/cmd/install/proto"
	"trishul/cmd/install/tools"

	"github.com/spf13/cobra"
)

var goroot, gopath, path string

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "This command is used to install various tools",
	Long: `This command can be used to install golang, protoc, etc.
trishul install go - starts the installation of golang
trishul install protoc - starts the installation of protoc`,
}

var goCmd = &cobra.Command{
	Use:   "go",
	Short: "installs golang",
	Long: `Usage:
trishul install go
	This command installs golang on the default goroot
	flags :
		--goroot : give go root
		--gopath : give gopath`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return golang.Install(goroot, gopath)
	},
}

var protoCmd = &cobra.Command{
	Use:   "protoc",
	Short: "installs protoc - the protobuf compiler",
	Long: `Usage:
trishul install protoc
	This command installs protoc in $GOPATH/bin
	flags :
		-p, --path : give custom path`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return proto.Install(path)
	},
}

var managerCmd = &cobra.Command{
	Use:   "manager",
	Short: "installs the required package manager",
	Long: `Usage:
trishul install manager
	This command installs : 
		chocolatey for windows
		homebrew for macOS`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return manager.Install()
	},
}

var gitCmd = &cobra.Command{
	Use:   "git",
	Short: "installs the git version control system",
	Long: `Usage:
trishul install git`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return git.Install()
	},
}

var toolsCmd = &cobra.Command{
	Use:   "tools",
	Short: "installs go tools",
	Long: `Usage:
trishul install tools
	This command installs : 
		goconvey
		mockery`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return tools.Install()
	},
}

func init() {

	rootCmd.AddCommand(installCmd)

	//Adding the subcommands
	installCmd.AddCommand(goCmd, protoCmd, managerCmd, gitCmd, toolsCmd)

	//Flags for goCmd
	goCmd.Flags().StringVar(&goroot, "goroot", goroot, "Takes user defined goroot")
	goCmd.Flags().StringVar(&gopath, "gopath", gopath, "Takes user defined gopath")

	//Flags for protoc
	// protoCmd.Flags().StringVarP(&path, "path", "p", path, "Takes user defined path for installation of protoc")
}
