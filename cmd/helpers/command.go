package helpers

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
	"os/exec"
)

//RunCommand runs a command on shell and returns the output of the command
//If there is an error, then it returns the error message, else the output of command
func RunCommand(name string, args ...string) (string, error) {
	var output, stderr bytes.Buffer

	cmd := exec.Command(name, args...)
	cmd.Stdout = &output
	cmd.Stderr = &stderr

	if err := cmd.Run(); err != nil {
		return stderr.String(), err
	}

	return output.String(), nil
}

//ComputeSHA computes and returns the SHA256 checksum of the input file
func ComputeSHA(filename string) (string, error) {
	hasher := sha256.New()

	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	if _, err := io.Copy(hasher, f); err != nil {
		return "", err
	}

	hash := hex.EncodeToString(hasher.Sum(nil))

	return hash, nil
}
