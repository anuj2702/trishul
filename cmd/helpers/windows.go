// +build windows

package helpers

import (
	"os"
	"os/exec"
	"path/filepath"
)

//HasRootAccess detects whether trishul is being run with admin privileges or not
func HasRootAccess() bool {
	if err := exec.Command("NET", "SESSION").Run(); err != nil {
		return false
	}

	return true
}

//GetDownloadPath creates and returns the default download path
func GetDownloadPath() (string, error) {
	downloadPath, err := filepath.Abs(os.Getenv("TEMP") + "/acli")
	if err != nil {
		return "", err
	}

	if _, err := os.Stat(downloadPath); os.IsNotExist(err) {
		if err := os.Mkdir(downloadPath, os.ModePerm); err != nil {
			return "", err
		}
	} else if err != nil {
		return "", err
	}

	return downloadPath, nil
}
