// +build darwin

package helpers

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
)

//HasRootAccess detects whether trishul is being run with root access or not
func HasRootAccess() bool {
	output, err := RunCommand("id", "-u")
	if err != nil {
		fmt.Println(err)
	}

	// output has trailing \n
	// need to remove the \n
	// otherwise it will cause error for strconv.Atoi
	// log.Println(output[:len(output)-1])

	// 0 = root, 501 = non-root user
	i, err := strconv.Atoi(output[:len(output)-1])
	if err != nil {
		fmt.Println(err)
	}

	if i != 0 {
		return false
	}

	return true
}

//GetDownloadPath creates and returns the default download path
func GetDownloadPath() (string, error) {
	downloadPath, err := filepath.Abs("/tmp/acli")
	if err != nil {
		return "", err
	}

	if _, err := os.Stat(downloadPath); os.IsNotExist(err) {
		if err := os.Mkdir(downloadPath, os.ModePerm); err != nil {
			return "", err
		}
	} else if err != nil {
		return "", err
	}

	return downloadPath, nil
}
