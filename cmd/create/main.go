package create

import (
	"errors"
	"fmt"
	"os"
	"strings"
)

//Execute executes the command and generates micro-service
func Execute(output, input string, pbOnly, forcibly bool) error {
	folderpath, protopath, err := ParsePaths(output, input)
	if err != nil {
		return err
	}

	if valid := validateFolderName(folderpath); !valid {
		return errors.New("invalid folder name : do not use underscores in folder name")
	}

	if err := DoesExists(folderpath, protopath); err != nil {
		return err
	}

	protoFiles, err := getProtoFilesFromFolder(protopath)
	if err != nil {
		return err
	}

	if len(protoFiles) == 0 {
		return fmt.Errorf("%v does not contain any proto files", input)
	}

	if ok, err := checkProtosBelongToSamePackage(protoFiles); err != nil {
		return err
	} else if !ok {
		return errors.New("The proto files does not belong to same package")
	}

	if err := os.Chdir(folderpath); err != nil {
		return err
	}

	//Generate pb files and check the flag to learn whether whole project has to be generated or only pb files
	if err := generatePbFiles(protopath, folderpath, protoFiles); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("pb files generated")
	}
	if pbOnly {
		return nil
	}

	if IsFolderGitRepo(folderpath) {
		if !forcibly {
			return errors.New("The output folder is already a git repository. Use --forcibly or -f flag to overwrite it")
		}
	}

	if err := generateFiles(protoFiles, protopath); err != nil {
		return err
	}
	for _, name := range protoFiles {
		fmt.Println(getLastNodeFromPath(name) + ".go" + " created")
	}
	fmt.Println("fx.go created")

	if err := createModFile(getLastNodeFromPath(folderpath)); err != nil {
		return err
	}
	fmt.Println("go.mod created")

	if err := createGitignoreFile(); err != nil {
		return err
	}
	fmt.Println(".gitignore created")

	if gitExists() {
		if err := createGitRepository(); err != nil {
			return err
		}
	} else {
		if err := createGitRepoByLibrary(folderpath); err != nil {
			return err
		}
	}
	fmt.Println("git repository initialized")

	return nil
}

func validateFolderName(folderpath string) bool {
	folderName := getLastNodeFromPath(folderpath)

	if contains := strings.Contains(folderName, "_"); !contains {
		return true
	}

	return false
}
