package create

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
)

//ParsePaths parses the path to required form
func ParsePaths(servicePath, protoPath string) (string, string, error) {
	abspath, err := filepath.Abs(servicePath)
	if err != nil {
		return "", "", err
	}

	protoPath, err = filepath.Abs(protoPath)
	if err != nil {
		return "", "", err
	}

	return abspath, protoPath, nil
}

//DoesExists checks whether the inputpath exists or not. If outputpath does not exists it creates it and changes
//current working dir to outputpath
func DoesExists(outputpath, inputpath string) error {
	if _, err := os.Stat(inputpath); os.IsNotExist(err) {
		return errors.New("path to proto folder does not exists")
	}

	if _, err := os.Stat(outputpath); os.IsNotExist(err) {
		if err := os.MkdirAll(outputpath, os.ModePerm); err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	return nil
}

//getLastNodeFromPath gets user from ../dir/users
func getLastNodeFromPath(folderpath string) string {
	nodeName := ""

	i := len(folderpath) - 1
	for ; i >= 0; i-- {
		if folderpath[i] == '/' || folderpath[i] == '\\' {
			i++
			break
		}
	}

	for ; i < len(folderpath); i++ {
		nodeName += string(folderpath[i])
	}

	if strings.HasSuffix(nodeName, ".proto") {
		nodeName = strings.Split(nodeName, ".proto")[0]
	}

	return nodeName
}
