package create

import (
	"fmt"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"io/ioutil"
	"os"
	"time"
	"trishul/cmd/helpers"
)

//createModFile creates the go.mod file
func createModFile(foldername string) error {
	data := []byte(fmt.Sprintf("module go.appointy.com/%v", foldername))
	if err := ioutil.WriteFile("go.mod", data, os.ModePerm); err != nil {
		fmt.Print("Error in creating go.mod : ")
		return err
	}

	return nil
}

//CreateGitignoreFile creates gitignore file to ignore .idea folder from commits
func createGitignoreFile() error {
	data := []byte("# Binaries for programs and plugins\n*.exe\n*.exe~\n*.dll\n*.so\n*.dylib\n\n" +
		"# Test binary, build with `go test -c`\n`*.test\n\n" +
		"# Output of the go coverage tool, specifically when used with LiteIDE\n*.out\n\n" +
		"# IDE Preferences\n.idea/\n.vscode/\n.vs/\n\n")
	if err := ioutil.WriteFile(".gitignore", data, os.ModePerm); err != nil {
		fmt.Print("Error in creating .gitignore : ")
		return err
	}

	return nil
}

//CreateGitRepository runs git init, git add and git commit commands
func createGitRepository() error {
	cmd := "git"

	args := []string{"init"}
	output, err := helpers.RunCommand(cmd, args...)
	if err != nil {
		return fmt.Errorf(output)
	}

	args = []string{"add", "."}
	output, err = helpers.RunCommand(cmd, args...)
	if err != nil {
		return fmt.Errorf(output)
	}

	args = []string{"commit", "-m", "Intial Commit"}
	output, err = helpers.RunCommand(cmd, args...)
	if err != nil {
		return fmt.Errorf(output)
	}

	return nil
}

//createGitRepoByLibrary creates the git repo using src-d git library
func createGitRepoByLibrary(folderpath string) error {
	r, err := git.PlainInit(folderpath, false)
	if err != nil {
		return err
	}

	repo, err := r.Worktree()
	if err != nil {
		return err
	}

	if _, err := repo.Add("."); err != nil {
		return err
	}

	if _, err := repo.Commit("Initial Commit", &git.CommitOptions{
		Author: &object.Signature{
			Name: "Trishul",
			When: time.Now(),
		},
	}); err != nil {
		return err
	}

	return nil
}

//IsFolderGitRepo checks whether the input path is git repo or not
func IsFolderGitRepo(path string) bool {
	if _, err := git.PlainOpen(path); err != nil {
		fmt.Println(err.Error())
		return false
	}

	return true
}

func gitExists() bool {
	if _, err := helpers.RunCommand("git", "config", "--list"); err != nil {
		return false
	}

	return true
}
