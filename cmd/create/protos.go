package create

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"trishul/cmd/helpers"

	"github.com/emicklei/proto"
)

//CheckProtosBelongToSamePackage checks all the proto files in proto folder belong to same package
func checkProtosBelongToSamePackage(protoPaths []string) (bool, error) {
	pName, err := getPackageNameOfProto(protoPaths[0])
	if err != nil {
		return false, err
	}

	for i := 1; i < len(protoPaths); i++ {
		name, err := getPackageNameOfProto(protoPaths[i])
		if err != nil {
			return false, err
		}

		if name != pName {
			return false, nil
		}
	}

	return true, nil
}

//GetPackageNameOfProto gets the package name from proto file
func getPackageNameOfProto(filepath string) (string, error) {
	reader, err := os.Open(filepath)
	if err != nil {
		return "", err
	}
	defer reader.Close()

	parser := proto.NewParser(reader)
	definition, err := parser.Parse()
	if err != nil {
		return "", fmt.Errorf("error in %v : %v", filepath, err.Error())
	}

	var name string
	for _, e := range definition.Elements {
		if p, ok := e.(*proto.Package); ok {
			name = p.Name
		}
	}

	return name, nil
}

//GetPackageNameFromImportPath extracts locations from go.appointy.com/google/pb/locations
func getPackageNameFromImportPath(importPath string) string {
	if importPath == "" {
		return importPath
	}

	i := len(importPath) - 1

	for ; i > 0; i-- {
		if importPath[i] == '/' {
			i++
			break
		}
	}

	packageName := ""
	for ; i < len(importPath); i++ {
		packageName += string(importPath[i])
	}

	return packageName
}

func getProtoFilesFromFolder(protopath string) ([]string, error) {
	if err := os.Chdir(protopath); err != nil {
		return nil, err
	}

	files, err := ioutil.ReadDir(protopath)
	if err != nil {
		return nil, err
	}

	protoPaths := make([]string, 0, len(files))

	for _, f := range files {
		filename, err := filepath.Abs(f.Name())
		if err != nil {
			return nil, err
		}

		if strings.HasSuffix(filename, ".proto") {
			protoPaths = append(protoPaths, filename)
		}
	}

	return protoPaths, nil
}

func generatePbFiles(protopath, folderpath string, protoFiles []string) error {
	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		return fmt.Errorf("$GOPATH not set")
	}

	args := make([]string, 0, 16)

	includePath, err := filepath.Abs(gopath + "/include/")
	if err != nil {
		return err
	}

	validatePath, err := filepath.Abs(gopath + "/src/github.com/lyft/protoc-gen-validate/")
	if err != nil {
		return err
	}

	gatewayPath, err := filepath.Abs(gopath + "/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis/")
	if err != nil {
		return err
	}

	args = append(args, fmt.Sprintf("--proto_path=%v", filepath.Dir(protopath)))
	args = append(args, fmt.Sprintf("--proto_path=%v", includePath))
	args = append(args, fmt.Sprintf("--proto_path=%v", validatePath))
	args = append(args, fmt.Sprintf("--proto_path=%v", gatewayPath))
	for _, file := range protoFiles {
		args = append(args, fmt.Sprintf("%v", file))
	}
	args = append(args, fmt.Sprintf("--go_out=plugins=grpc:%v", folderpath))

	if output, err := helpers.RunCommand("protoc", args...); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}
