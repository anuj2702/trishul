package create

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/emicklei/proto"
)

var wellKnownImports []string
var packageName, importPath string
var code, structName, fxCode string

//GenerateFiles creates the required go files
func generateFiles(protoFiles []string, protopath string) error {
	goFilePackage := getLastNodeFromPath(protopath)

	firstTime := true
	for _, proto := range protoFiles {
		code = ""
		structName = getLastNodeFromPath(proto) + "Server"

		if err := parseProtos(proto); err != nil {
			return err
		}

		data := getCoreCode(firstTime, goFilePackage)
		if err := ioutil.WriteFile(getLastNodeFromPath(proto)+".go", data, os.ModePerm); err != nil {
			fmt.Printf("Error in creating  %v.go file: ", getLastNodeFromPath(proto))
			return err
		}
		firstTime = false

	}

	return generateFxFile(goFilePackage)
}

func generateFxFile(goFilePackage string) error {
	code := fmt.Sprintf("package %v\n\n", goFilePackage)
	code += "import \"go.uber.org/fx\"\n\n// Module is the fx module encapsulating all the providers of the package \nvar Module = fx.Provide("
	code += fxCode
	code += "\n)"

	if err := ioutil.WriteFile("fx.go", []byte(code), os.ModePerm); err != nil {
		fmt.Print("Error in creating fx.go file: ")
		return err
	}

	return nil
}

func getCoreCode(firstTime bool, goFilePackage string) []byte {
	coreCode := fmt.Sprintf("package %v\n\nimport (\n\t\"context\"\n", goFilePackage)
	if firstTime {
		coreCode += "\t\"google.golang.org/grpc/codes\"\n\t\"google.golang.org/grpc/status\"\n"
	}
	coreCode += fmt.Sprintf("\tpb \"%v\"", importPath)
	for _, imp := range wellKnownImports {
		coreCode += "\n\t"
		coreCode += fmt.Sprintf(`"%v"`, imp)
	}
	coreCode += "\n)\n\n"

	if firstTime {
		coreCode += "var (\n\t"
		coreCode += "errInternal = status.Error(codes.Internal, `Oops! Something went wrong`) // Generic Error to be returned to client to hide possible sensitive information\n)\n\n"
	}

	coreCode += fmt.Sprintf("type %v struct {\n\t//add store\n\t//add other clients\n}\n\n", structName)
	coreCode += code

	return []byte(coreCode)
}

//ParseProtos parses the protos and generate the required code
func parseProtos(protopath string) error {
	reader, err := os.Open(protopath)
	if err != nil {
		return err
	}
	defer reader.Close()

	parser := proto.NewParser(reader)
	definition, err := parser.Parse()
	if err != nil {
		return err
	}

	proto.Walk(definition,
		proto.WithOption(handleOption),
		proto.WithService(handleService),
		proto.WithRPC(handleRPC),
	)

	return nil
}

func handleOption(o *proto.Option) {
	if o.Name == "go_package" {
		importPath = o.Constant.Source
		packageName = "pb"
	}
}

func handleService(s *proto.Service) {
	code += fmt.Sprintf("//New%vServer returns a %vServer implementation with core business logic", s.Name, s.Name)
	code += fmt.Sprintf("\nfunc New%vServer() %v.%vServer {\n\treturn &%v{}\n}\n\n", s.Name, packageName, s.Name, structName)

	fxCode += fmt.Sprintf("\n\tNew%vServer,", s.Name)

}

func handleRPC(rpc *proto.RPC) {
	reqPackge, req := parseMessage(rpc.RequestType)
	resPackage, res := parseMessage(rpc.ReturnsType)

	if rpc.Comment != nil {
		for _, c := range rpc.Comment.Lines {
			code += fmt.Sprintf("//%v \n", c)
		}
	} else {
		code += fmt.Sprintf("//%v ...\n", rpc.Name)
	}
	code += fmt.Sprintf("func (s *%v) %v (ctx context.Context, in *%v.%v) ( *%v.%v, error){\n\tpanic(`Implement me`)\n}\n\n",
		structName, rpc.Name, reqPackge, req, resPackage, res)
}

//ParseMessage takes the message and returns the import and struct.
func parseMessage(message string) (string, string) {
	if exists := strings.ContainsRune(message, '.'); !exists {
		return packageName, message
	}

	//A well known message used
	KnownMessages := populateKnownMessagesMap()

	if message[0] == '.' {
		message = message[1:]
	}

	importNeeded := KnownMessages[message]
	exists := false
	for _, imp := range wellKnownImports {
		if imp == importNeeded {
			exists = true
			break
		}
	}

	if !exists {
		wellKnownImports = append(wellKnownImports, importNeeded)
	}

	return getPackageNameFromImportPath(importNeeded), getStructFromMessage(message)
}

//PopulateKnownMessagesMap populates the map required to determine imports
func populateKnownMessagesMap() map[string]string {
	KnownMessages := make(map[string]string, 10)
	KnownMessages["google.protobuf.Any"] = "github.com/golang/protobuf/ptypes/any"
	KnownMessages["google.protobuf.Duration"] = "github.com/golang/protobuf/ptypes/duration"
	KnownMessages["google.protobuf.Empty"] = "github.com/golang/protobuf/ptypes/empty"
	KnownMessages["google.protobuf.Struct"] = "github.com/golang/protobuf/ptypes/struct"
	KnownMessages["google.protobuf.ListValue"] = "github.com/golang/protobuf/ptypes/struct"
	KnownMessages["google.protobuf.Value"] = "github.com/golang/protobuf/ptypes/struct"
	KnownMessages["google.protobuf.Timestamp"] = "github.com/golang/protobuf/ptypes/timestamp"
	KnownMessages["google.protobuf.DoubleValue"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.FloatValue"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.Int64Value"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.UInt64Value"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.Int32Value"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.UInt32Value"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.BoolValue"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.StringValue"] = "github.com/golang/protobuf/ptypes/wrappers"
	KnownMessages["google.protobuf.BytesValue"] = "github.com/golang/protobuf/ptypes/wrappers"

	return KnownMessages
}

//GetStructFromMessage gets Empty from google.protobuf.Empty
func getStructFromMessage(message string) string {
	i := len(message) - 1

	for ; i > 0; i-- {
		if message[i] == '.' {
			i++
			break
		}
	}

	structName := ""
	for ; i < len(message); i++ {
		structName += string(message[i])
	}

	return structName
}
