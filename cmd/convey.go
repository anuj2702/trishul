package cmd

import (
	"trishul/cmd/convey"

	"github.com/spf13/cobra"
)

var inputpath, outputpath string

// conveyCmd represents the create command
var conveyCmd = &cobra.Command{
	Use:   "convey",
	Short: "Used to  generate test boilerplate code",
	Long: `Used to generate test boiler-plate code required for testing using convey. 
It takes the path of the folder containing spec files as input and geerates corresponding test files in output folder. For example:

trishul convey -i ./proto -o ./users `,
	Args: cobra.MaximumNArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		return convey.Execute(inputpath, outputpath)
	},
}

func init() {
	rootCmd.AddCommand(conveyCmd)

	conveyCmd.Flags().StringVarP(&outputpath, "input", "i", outputpath, "This flag takes the path of input folder")
	conveyCmd.Flags().StringVarP(&inputpath, "output", "o", inputpath, "This flag takes the path of output folder")
	conveyCmd.MarkFlagRequired("input")
	conveyCmd.MarkFlagRequired("output")
}
