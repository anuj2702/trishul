package cmd

import (
	"trishul/cmd/analyze"

	"github.com/spf13/cobra"
)

var verbose bool

// analyzeCmd represents the command that checks the source code
var analyzeCmd = &cobra.Command{
	Use:   "analyze",
	Short: "Checks the source code using various packages",
	Long:  `trishul analyze package1 package2`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return analyze.Execute(args, verbose)
	},
	Args: cobra.MinimumNArgs(1),
}

func init() {
	rootCmd.AddCommand(analyzeCmd)

	analyzeCmd.Flags().BoolVarP(&verbose, "verbose", "v", false, "This flag is used to tooggle verbose flag while testing")
}
