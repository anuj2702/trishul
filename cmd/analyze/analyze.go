//Package analyze tests the source code using various tools and builds it
package analyze

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"trishul/cmd/helpers"
)

//Execute is used to run various commands on all the entered packages
func Execute(packages []string, verbose bool) error {
	defaultTomlPath, err := installRevive()
	if err != nil {
		return err
	}

	invocationDir, err := os.Getwd()
	if err != nil {
		return err
	}

	for _, p := range packages {
		path, err := filepath.Abs(p)
		if err != nil {
			fmt.Println(err)
			continue
		}

		analyze(path, defaultTomlPath, verbose)

		if err := os.Chdir(invocationDir); err != nil {
			return err
		}
	}

	return nil
}

//Analyze analyzes the package in path using gofmt,go vet,go get, revive, go test
//Analyze changes the Chdir to path
func analyze(path, defaultTomlPath string, verbose bool) {
	fmt.Printf("Analyzing %v\n", path)

	//Change the current working directory
	if err := os.Chdir(path); err != nil {
		fmt.Println(err)
		return
	}

	//Go get the dependencies
	output, _ := helpers.RunCommand("go", "get")
	fmt.Print(output)

	//Run go vet command to scan code for various unwise constructs
	output, _ = helpers.RunCommand("go", "vet")
	fmt.Print(output)

	//Run gofmt to format the code
	output, _ = helpers.RunCommand("gofmt", "-w", ".")
	fmt.Print(output)

	//Linting
	output, _ = helpers.RunCommand("revive", "-config", defaultTomlPath, "-formatter", "default", "./...")
	fmt.Print(output)

	//Run the testcases present
	output = runTests(verbose)
	fmt.Print(output)

	fmt.Println("Analysis Complete")
}

//RunTests runs the test files present in current package and takes a flag verbose
func runTests(verbose bool) string {
	args := []string{"test"}
	if verbose {
		args = append(args, "-v")
	}
	args = append(args, "-timeout", "30s", "-cover", ".")

	output, _ := helpers.RunCommand("go", args...)
	return output
}

//InstallRevive installs the revive linter if it does not exists
//It also creates the defaults.toml file needed to lint the code and returns its path
func installRevive() (string, error) {
	var trishulPath string
	if path := os.Getenv("SystemDrive"); path != "" {
		trishulPath = path
	} else {
		trishulPath = `/usr/local`
	}

	trishulPath, err := filepath.Abs(trishulPath + `/trishul`)
	if err != nil {
		return "", err
	}

	if _, err := os.Stat(trishulPath); os.IsNotExist(err) {
		if err := os.Mkdir(trishulPath, os.ModePerm); err != nil {
			return "", err
		}

		if err := os.Chdir(trishulPath); err != nil {
			return "", err
		}

		//create defaults.toml file
		if err := ioutil.WriteFile("defaults.toml", []byte(defaultToml), os.ModePerm); err != nil {
			return "", err
		}
	}

	tomlPath, err := filepath.Abs(trishulPath + `/defaults.toml`)
	if err != nil {
		return "", err
	}

	reader, err := os.Open(tomlPath)
	if os.IsNotExist(err) {
		if err := os.Chdir(trishulPath); err != nil {
			return "", err
		}

		//create defaults.toml file
		if err := ioutil.WriteFile("defaults.toml", []byte(defaultToml), os.ModePerm); err != nil {
			return "", err
		}
	}
	if err := reader.Close(); err != nil {
		return "", err
	}

	out, err := helpers.RunCommand("go", "get", "github.com/mgechev/revive")
	if err != nil {
		err = fmt.Errorf(out)
	}
	return tomlPath, err
}

const defaultToml = `ignoreGeneratedHeader = false
severity = "warning"
confidence = 0.8
errorCode = 0
warningCode = 0

[rule.blank-imports]
[rule.context-as-argument]
[rule.context-keys-type]
[rule.dot-imports]
[rule.error-return]
[rule.error-strings]
[rule.error-naming]
[rule.exported]
[rule.if-return]
[rule.increment-decrement]
[rule.var-naming]
[rule.var-declaration]
[rule.package-comments]
[rule.range]
[rule.receiver-naming]
[rule.time-naming]
[rule.unexported-return]
[rule.indent-error-flow]
[rule.errorf]
`
