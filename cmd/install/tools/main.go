package tools

import (
	"fmt"
	"trishul/cmd/helpers"
)

//Install installs various tools such as protoc-gen-validate, gotools, goconvey
func Install() error {
	fmt.Println("downloading go tools")
	if err := installGoTools(); err != nil {
		return err
	}

	fmt.Println("downloading goconvey")
	if err := installGoConvey(); err != nil {
		return err
	}

	fmt.Println("downloading mockery")
	return installMockery()
}

//InstallGoTools gets the tools
func installGoTools() error {
	output, err := helpers.RunCommand("go", "get", "-u", "golang.org/x/tools/cmd/...")
	if err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func installGoConvey() error {
	if output, err := helpers.RunCommand("go", "get", "github.com/smartystreets/goconvey"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func installMockery() error {
	if output, err := helpers.RunCommand("go", "get", "github.com/vektra/mockery/.../"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}
