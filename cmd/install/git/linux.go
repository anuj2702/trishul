// +build linux

package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"trishul/cmd/helpers"
)

func installGit() error {
	cmds := fmt.Sprintln("sudo apt-get --yes install git")
	if err := ioutil.WriteFile("git.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("/bin/sh", "git.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func checkPrerequisites() bool {
	if _, err := helpers.RunCommand("apt-get", "update"); err != nil {
		return false
	}

	return true
}
