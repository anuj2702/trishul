// +build windows

package git

import (
	"fmt"
	"trishul/cmd/helpers"
)

func installGit() error {
	if output, err := helpers.RunCommand("choco", "install", "git.install"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func checkPrerequisites() bool {
	if _, err := helpers.RunCommand("choco", "--version"); err != nil {
		return false
	}

	return true
}
