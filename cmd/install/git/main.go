package git

import (
	"os"
	"trishul/cmd/helpers"
	"trishul/cmd/install/manager"
)

//Install executes the setup command
func Install() error {
	downloadPath, err := helpers.GetDownloadPath()
	if err != nil {
		return err
	}

	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	if exists := checkPrerequisites(); !exists {
		if err := manager.Install(); err != nil {
			return err
		}
	}

	return installGit()
}
