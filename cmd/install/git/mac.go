// +build darwin

package git

import (
	"fmt"
	"io/ioutil"
	"os"
	"trishul/cmd/helpers"
)

func installGit() error {
	cmds := fmt.Sprintf("brew install git\n")
	if err := ioutil.WriteFile("git.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("sh", "git.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func checkPrerequisites() bool {
	if _, err := helpers.RunCommand("brew", "--version"); err != nil {
		return false
	}

	return true
}
