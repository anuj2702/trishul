// +build linux

package proto

import (
	"fmt"
	"io/ioutil"
	"os"
	"trishul/cmd/helpers"
)

func installProtoc(downloadPath, installationPath string) error {
	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	if err := helpers.DownloadFile("proto.zip", `https://gitlab.com/anuj2702/softwares/raw/master/protoc-linux.zip`); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("unzip", "proto.zip"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		return fmt.Errorf("$GOPATH not set")
	}

	cmds := fmt.Sprintf("cp ./bin/protoc %v/bin/protoc\n", gopath)
	cmds += fmt.Sprintf("yes | cp -rf ./include %v\n", gopath)

	if err := ioutil.WriteFile("cmds.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("sh", "cmds.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}
