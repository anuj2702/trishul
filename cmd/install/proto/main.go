package proto

import (
	"fmt"
	"os"
	"trishul/cmd/helpers"
	"trishul/cmd/install/golang"
)

//Install executes the command to install protocol buffers
func Install(path string) error {
	//Install golang if not installed
	if _, err := helpers.RunCommand("go", "version"); err != nil {
		if err := golang.Install("", ""); err != nil {
			return err
		}
	}

	downloadPath, installationPath, err := initPaths(path)
	if err != nil {
		return err
	}

	fmt.Println("downloading protoc")
	if err := installProtoc(downloadPath, installationPath); err != nil {
		return err
	}

	fmt.Println("downloading protoc-gen-go")
	if err := getProtoTools(); err != nil {
		return err
	}

	fmt.Println("downloading protoc-gen-grpc-gateway")
	if err := installGrpcGateway(); err != nil {
		return err
	}

	fmt.Println("downloading protoc-gen-validate")
	return installValidate()
}

//InitPaths intializes installation path and returns os type
func initPaths(path string) (string, string, error) {
	var installationPath string

	if path != "" {
		installationPath = path //TODO : add to $PATH installationPath/bin
	} else {
		installationPath = os.Getenv("GOPATH")
	}

	var err error
	downloadPath, err := helpers.GetDownloadPath()
	if err != nil {
		return "", "", err
	}

	return downloadPath, installationPath, nil
}

func getProtoTools() error {
	if output, err := helpers.RunCommand("go", "get", "-u", "github.com/golang/protobuf/protoc-gen-go"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func installValidate() error {
	if output, err := helpers.RunCommand("go", "get", "github.com/lyft/protoc-gen-validate"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}

func installGrpcGateway() error {
	if output, err := helpers.RunCommand("go", "get", "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	if output, err := helpers.RunCommand("go", "get", "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return nil
}
