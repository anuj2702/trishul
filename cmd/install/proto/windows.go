// +build windows

package proto

import (
	"fmt"
	"os"
	"trishul/cmd/helpers"
)

func installProtoc(downloadPath, installationPath string) error {
	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	if err := helpers.DownloadFile("proto.zip", `https://gitlab.com/anuj2702/softwares/raw/master/protoc-win.zip?inline=false`); err != nil {
		return err
	}

	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		return fmt.Errorf("$GOPATH not set")
	}

	if output, err := helpers.RunCommand("tar", "-zxvf", "proto.zip", "-C", gopath); err != nil {
		return fmt.Errorf(output)
	}

	return nil
}
