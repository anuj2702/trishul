// +build windows

package manager

import (
	"fmt"
	"io/ioutil"
	"os"
	"trishul/cmd/helpers"
)

//Install installs the choco package manager
func Install() error {
	downloadPath, err := helpers.GetDownloadPath()
	if err != nil {
		return err
	}

	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	if err := ioutil.WriteFile(`one.bat`, []byte(`powershell -command "Start-Process two.bat -Verb runas"`), os.ModePerm); err != nil {
		return err
	}

	if err := ioutil.WriteFile(`two.bat`, []byte(`@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"`), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("one.bat"); err != nil {
		fmt.Println(output)
		return err
	}

	return nil
}
