// +build darwin

package manager

import (
	"fmt"
	"io/ioutil"
	"os"
	"trishul/cmd/helpers"
)

//Install installs the brew package manager for macOS
func Install() error {
	downloadPath, err := helpers.GetDownloadPath()
	if err != nil {
		return err
	}

	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	if err := ioutil.WriteFile(`cmd.sh`, []byte(`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("/bin/sh", "cmd.sh"); err != nil {
		fmt.Println(output)
		return err
	}

	return nil
}
