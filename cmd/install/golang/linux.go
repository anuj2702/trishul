// +build linux

package golang

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"trishul/cmd/helpers"
)

//InitPaths returns path for downloading setup and sets goroot and gopath to default if empty
func initPaths(goroot, gopath *string) (string, error) {
	var downloadPath string

	downloadPath, err := helpers.GetDownloadPath()
	if err != nil {
		return "", err
	}

	if *goroot == "" {
		*goroot, err = filepath.Abs("/usr/local")
		if err != nil {
			return "", err
		}
	}

	if *gopath == "" {
		*gopath, err = filepath.Abs(os.Getenv("HOME") + "/go")
		if err != nil {
			return "", err
		}
	}

	gopathBin, err := filepath.Abs(*gopath + "/bin")
	if err != nil {
		return "", err
	}

	if err := os.MkdirAll(gopathBin, os.ModePerm); err != nil {
		return "", err
	}

	return downloadPath, nil
}

func getOsVersion() string {
	return "linux"
}

func installGolang(info *setup, goroot, gopath string) error {
	cmds := fmt.Sprintf("sudo tar -xzvf %v -C %v\n", info.filename, goroot)
	if err := ioutil.WriteFile("cmd.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("/bin/sh", "cmd.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return addPaths(goroot, gopath)
}

//To add an environment variable in linux add it to ~/.bashrc
func addPaths(goroot, gopath string) error {
	goroot, err := filepath.Abs(goroot + "/go")
	if err != nil {
		return err
	}

	cmds := fmt.Sprintln("echo", fmt.Sprintf("export GOROOT=%v", goroot), "|", "cat", ">>", "~/.bashrc")
	cmds += fmt.Sprintln("echo", fmt.Sprintf("export GOPATH=%v", gopath), "|", "cat", ">>", "~/.bashrc")
	if err := ioutil.WriteFile("go.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("/bin/sh", "go.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}
	fmt.Println("GOROOT setup to ", goroot)
	fmt.Println("GOPATH setup to ", gopath)

	//Add GOBIN to path
	gobin, err := filepath.Abs(goroot + "/bin")
	if err != nil {
		return err
	}

	gopathBin, err := filepath.Abs(gopath + "/bin")
	if err != nil {
		return err
	}

	cmds = fmt.Sprintln("echo", fmt.Sprintf("'PATH=$PATH:%v'", gobin), "|", "cat", ">>", "~/.profile")
	cmds += fmt.Sprintln("echo", fmt.Sprintf("'PATH=$PATH:%v'", gopathBin), "|", "cat", ">>", "~/.profile")
	if err := ioutil.WriteFile("go.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("/bin/sh", "go.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}
	fmt.Println("adding ", gobin, " to $PATH")

	return nil
}
