package golang

import (
	"errors"
	"fmt"
	"os"
	"trishul/cmd/helpers"
)

//Install executes the install command
//Is downloads the latest version of golang for the required platform and installs it along with golang tools
func Install(goroot, gopath string) error {
	if _, err := helpers.RunCommand("go", "version"); err == nil {
		return errors.New("go already installed")
	}

	downloadPath, err := initPaths(&goroot, &gopath)
	if err != nil {
		return err
	}

	links, err := extractInfo()
	if err != nil {
		return err
	}

	if err := os.Chdir(downloadPath); err != nil {
		return err
	}

	os := getOsVersion()

	fmt.Println("downloading ", links[os].filename)
	if err := downloadGolang(links[os]); err != nil {
		return err
	}

	fmt.Println("installing ", links[os].filename)

	return installGolang(links[os], goroot, gopath)
}

//DownloadGolang downloads the required setup and checks its sha256 checksum to ensure its correctness
func downloadGolang(info *setup) error {
	if err := helpers.DownloadFile(info.filename, info.url); err != nil {
		return err
	}

	checksum, err := helpers.ComputeSHA(info.filename)
	if err != nil {
		return err
	}

	if checksum != info.hash {
		return fmt.Errorf("Checksum do not match")
	}

	return nil
}
