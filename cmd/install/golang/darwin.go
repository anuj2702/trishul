// +build darwin

package golang

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"trishul/cmd/helpers"
)

//InitPaths returns path for downloading setup and sets goroot and gopath to default if empty
func initPaths(goroot, gopath *string) (string, error) {
	var err error

	if *goroot == "" {
		*goroot, err = filepath.Abs("/usr/local")
		if err != nil {
			return "", err
		}
	}

	if *gopath == "" {
		*gopath, err = filepath.Abs(os.Getenv("HOME") + "/go")
		if err != nil {
			return "", err
		}
	}

	gopathBin, err := filepath.Abs(*gopath + "/bin")
	if err != nil {
		return "", err
	}

	if err := os.MkdirAll(gopathBin, os.ModePerm); err != nil {
		return "", err
	}

	return helpers.GetDownloadPath()
}

func getOsVersion() string {
	return "mac"
}

func installGolang(info *setup, goroot, gopath string) error {
	cmds := fmt.Sprintf("sudo installer -pkg ./%v -target /\n", info.filename)
	if err := ioutil.WriteFile("cmd.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("sh", "cmd.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}

	return addPaths(goroot, gopath)
}

//To add an environment variable in darwin add it to ~/.bash_profile and gobin to /etc/paths
func addPaths(goroot, gopath string) error {
	goroot, err := filepath.Abs(goroot + "/go")
	if err != nil {
		return err
	}

	cmds := fmt.Sprintln("echo", fmt.Sprintf("export GOROOT=%v", goroot), "|", "cat", ">>", "~/.bash_profile")
	cmds += fmt.Sprintln("echo", fmt.Sprintf("export GOPATH=%v", gopath), "|", "cat", ">>", "~/.bash_profile")
	if err := ioutil.WriteFile("goenv.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("sh", "goenv.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}
	fmt.Println("GOROOT setup to ", goroot)
	fmt.Println("GOPATH setup to ", gopath)

	//Add GOBIN to path
	gobin, err := filepath.Abs(goroot + "/bin")
	if err != nil {
		return err
	}

	gopathBin, err := filepath.Abs(gopath + "/bin")
	if err != nil {
		return err
	}

	cmds = fmt.Sprintln("echo", fmt.Sprintf(gobin), "|", "sudo", "cat", ">>", "go")
	cmds += fmt.Sprintln("echo", fmt.Sprintf(gopathBin), "|", "sudo", "cat", ">>", "go")
	cmds += "sudo cp go /etc/paths.d/go"
	if err := ioutil.WriteFile("gobin.sh", []byte(cmds), os.ModePerm); err != nil {
		return err
	}

	if output, err := helpers.RunCommand("sh", "gobin.sh"); err != nil {
		return fmt.Errorf(output + " " + err.Error())
	}
	fmt.Println("adding ", gobin, " to $PATH")

	return nil
}
