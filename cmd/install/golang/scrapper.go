package golang

import (
	"io/ioutil"
	"net/http"
	"strings"
)

//ExtractInfo extracts the required information for downloading golang setup of various platforms
//It scrapes the download link, filename and sha256 checksum of the setups
func extractInfo() (map[string]*setup, error) {
	resp, err := http.Get("https://golang.org/dl/")
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	body := string(data[:])
	downloadLinks := extractDownloadLinks(body)
	extractSHAs(body, downloadLinks)

	return downloadLinks, nil
}

//ExtractDownloadLinks extracts the downloadlink,filename of the setups from the golang download page source code
func extractDownloadLinks(body string) map[string]*setup {
	downloadLinks := make(map[string]*setup, 3)

	index1 := strings.Index(body, "Featured downloads")
	index2 := strings.Index(body, "Stable versions")
	body = body[index1:index2]

	//Extract windows information
	index1 = strings.Index(body, "<a ")
	index2 = strings.Index(body, "</a>")
	downloadLinks["windows"] = extractLinkFromHTML(body[index1:index2])
	body = body[index2+5:]

	//Extract macOS information
	index1 = strings.Index(body, "<a ")
	index2 = strings.Index(body, "</a>")
	downloadLinks["mac"] = extractLinkFromHTML(body[index1:index2])
	body = body[index2+5:]

	//Extract linux information
	index1 = strings.Index(body, "<a ")
	index2 = strings.Index(body, "</a>")
	downloadLinks["linux"] = extractLinkFromHTML(body[index1:index2])

	return downloadLinks
}

//ExtractLinkFromHTML extracts the required information from a piece of code which belongs to a platform
func extractLinkFromHTML(html string) *setup {
	link := setup{}

	url := html[strings.Index(html, `https`):]
	url = url[:strings.Index(url, `">`)]
	link.url = url

	os := html[strings.Index(html, `<div class="platform">`)+22:]
	os = os[:strings.Index(os, `</div>`)]
	link.os = os

	filename := html[strings.Index(html, `<span class="filename">`)+23:]
	filename = filename[:strings.Index(filename, `</span>`)]
	link.filename = filename

	return &link
}

//ExtractSHAs extracts the sha256 checksum of required setup files
func extractSHAs(body string, downloadLinks map[string]*setup) {
	index1 := strings.Index(body, "Stable versions")
	index2 := strings.Index(body, "Archived versions")
	body = body[index1:index2]

	for _, value := range downloadLinks {
		value.hash = extractSHAFromHTML(body, value.filename)
	}
}

//ExtractSHAFromHTML extracts the sha of a given file from the golang download page source code
func extractSHAFromHTML(html, filename string) string {
	html = html[strings.Index(html, filename):]
	html = html[:strings.Index(html, `</tt></td>`)]

	return html[strings.Index(html, `<td><tt>`)+8:]
}

//Setup encapsulates the required information
type setup struct {
	os       string
	filename string
	url      string
	hash     string
}
