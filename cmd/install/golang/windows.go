// +build windows

package golang

import (
	"fmt"
	"time"
	"trishul/cmd/helpers"
)

//InitPaths returns path for downloading setup and sets goroot and gopath to default if empty
//No need to setup gopath and goroot in windows as the installer will take care of it
func initPaths(goroot, gopath *string) (string, error) {
	return helpers.GetDownloadPath()
}

func getOsVersion() string {
	return "windows"
}

func installGolang(info *setup, goroot, gopath string) error {
	if output, err := helpers.RunCommand("msiexec", "/i", info.filename); err != nil {
		return fmt.Errorf("%v : %v", err, output)
	}

	time.Sleep(time.Minute)

	return nil
}
