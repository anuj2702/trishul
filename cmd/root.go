package cmd

import (
	"fmt"
	"os"
	"trishul/cmd/helpers"

	"github.com/spf13/cobra"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "trishul",
	Short: "Appointy command line interface",
	Long:  `Trishul is a command line interface developed to bring together the installation process and generation of boilerplate code together.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if helpers.HasRootAccess() {
		fmt.Println("Do not run with admin privileges / root access")
		os.Exit(1)
	}

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
