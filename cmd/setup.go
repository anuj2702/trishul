package cmd

import (
	"trishul/cmd/setup"

	"github.com/spf13/cobra"
)

var name, email string

// setupCmd represents the setup command
var setupCmd = &cobra.Command{
	Use:   "setup",
	Short: "Downloads and install git if it does not exist, Setup name and email",
	Long:  `trishul setup gitlab -n name -e email`,
	RunE: func(cmd *cobra.Command, args []string) error {
		return setup.Execute(name, email)
	},
}

func init() {
	rootCmd.AddCommand(setupCmd)

	setupCmd.Flags().StringVarP(&name, "name", "n", name, "This flag takes name as input")
	setupCmd.Flags().StringVarP(&email, "email", "e", email, "This flag takes email as input")
	setupCmd.MarkFlagRequired("name")
	setupCmd.MarkFlagRequired("email")
}
