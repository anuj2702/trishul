package setup

import (
	"fmt"
	"trishul/cmd/helpers"
)

//Execute executes the setup command
func Execute(name, email string) error {
	return setupDetails(name, email)
}

func setupDetails(name, email string) error {
	cmd := "git"

	args := []string{"config", "--global", "user.name"}
	args = append(args, name)
	if output, err := helpers.RunCommand(cmd, args...); err != nil {
		return fmt.Errorf(output)
	}

	args = []string{"config", "--global", "user.email"}
	args = append(args, email)
	if output, err := helpers.RunCommand(cmd, args...); err != nil {
		return fmt.Errorf(output)
	}

	return nil
}
