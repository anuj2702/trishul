package cmd

import (
	"fmt"
	"trishul/cmd/create"

	"github.com/spf13/cobra"
)

var protopath, folderpath string
var pbOnly, forcibly bool

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Used to create microservice and boiler-plate code",
	Long: `Used to generate folder structure and boiler-plate code required to setup a micro service. 
It takes the path of the microservice as argument and path of proto as flag. For example:

trishul create -i ./proto -o ./users `,
	Args: cobra.MaximumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		if err := create.Execute(folderpath, protopath, pbOnly, forcibly); err != nil {
			fmt.Println(err.Error())
		}
	},
}

func init() {
	rootCmd.AddCommand(createCmd)

	createCmd.Flags().StringVarP(&protopath, "input", "i", protopath, "This flag takes the path of input proto folder")
	createCmd.Flags().StringVarP(&folderpath, "output", "o", folderpath, "This flag takes the path of output folder")
	createCmd.Flags().BoolVarP(&pbOnly, "pb_only", "p", pbOnly, "This flag is used to generate pb fies only")
	createCmd.Flags().BoolVarP(&forcibly, "force", "f", forcibly, "This flag is used to forcibly overwrite the existing files")
	createCmd.MarkFlagRequired("input")
	createCmd.MarkFlagRequired("output")
}
