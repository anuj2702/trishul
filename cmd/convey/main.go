package convey

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"trishul/cmd/create"
)

//Execute generates the testcases for all the files in input folder
func Execute(input, output string) error {
	outputpath, inputpath, err := create.ParsePaths(output, input)
	if err != nil {
		return err
	}

	if err := create.DoesExists(outputpath, inputpath); err != nil {
		return err
	}

	files, err := getSpecFilesFromFolder(inputpath)
	if err != nil {
		return err
	}

	for _, specfile := range files {
		gofile := filepath.Base(specfile) + ".go"
		if err := generateTests(specfile, gofile); err != nil {
			fmt.Println(specfile, " : ", err.Error())
		}
	}

	return nil
}

func getSpecFilesFromFolder(path string) ([]string, error) {
	if err := os.Chdir(path); err != nil {
		return nil, err
	}

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	paths := make([]string, 0, len(files))

	for _, f := range files {
		filename, err := filepath.Abs(f.Name())
		if err != nil {
			return nil, err
		}

		if strings.HasSuffix(filename, ".txt") {
			paths = append(paths, filename)
		}
	}

	return paths, nil
}
