package convey

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func generateTests(inputFile, outputFile string) error {
	lines, err := readAndBreak(inputFile)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(outputFile, []byte(generateCode(lines)), os.ModePerm)
}

func readAndBreak(filename string) ([]*testcase, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(data[:]), "\n")
	processedLines := make([]*testcase, 0, len(lines))

	for _, line := range lines {
		node := &testcase{
			data:  strings.TrimSpace(line),
			depth: numberOfTabs(line),
		}

		if strings.HasPrefix(line, "Test") {
			node.isRoot = true
		}

		processedLines = append(processedLines, node)
	}

	return processedLines, nil
}

func numberOfTabs(line string) int {
	count := 0
	length := len(line)

	for i := 0; i < length && line[i] == '\t'; i++ {
		count++
	}

	return count
}

func generateCode(lines []*testcase) string {
	code := `import (` + "\n" + `	"testing"` + "\n" + `	. "github.com/smartystreets/goconvey/convey"` + "\n" + `)` + "\n\n"
	top = -1

	for i := 0; i < len(lines); i++ {
		line := lines[i]

		if line.data == "" {
			code += "\n"
			continue
		}

		if peek() != nil && line.depth <= peek().depth {
			for {
				node := pop()
				if node == nil {
					break
				}

				code += strings.Repeat("\t", node.depth)
				if node.depth != 0 {
					code += "})\n"
				} else {
					code += ")\n"
				}

				if peek() == nil || line.depth > peek().depth {
					break
				}
			}
		}

		if line.isRoot {
			code += fmt.Sprintf("func %v( t *testing.T) {\n", line.data)
		} else {
			code += strings.Repeat("\t", line.depth)

			if line.depth == 1 {
				code += fmt.Sprintf("Convey(\"%v\", t, func(){\n", line.data)
			} else {
				code += fmt.Sprintf("Convey(\"%v\",  func(){\n", line.data)
			}
		}
		push(line)
	}

	node := pop()
	for node != nil {
		code += strings.Repeat("\t", node.depth)
		code += "})\n"

		node = pop()
	}

	return code
}

type testcase struct {
	data   string
	depth  int
	isRoot bool
}

var stack []*testcase
var top int32

func push(ptr *testcase) {
	top++
	stack = append(stack, ptr)
}

func pop() *testcase {
	if top == -1 {
		return nil
	}

	node := stack[top]
	if top != -1 {
		stack = stack[:top]
	}

	top--
	return node
}

func peek() *testcase {
	if top == -1 {
		return nil
	}

	return stack[top]
}

//Algorithm used in generateCode
/*
initialize empty stack

for each line in spec file
do
	if the number of tabs in line is more than number of tabs on top then
		push the line
	else
		pop the stack until number of tabs is less than number of tabs in line
done
*/
