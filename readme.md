# Trishul

Trishul is appointy command line interface.

**Install** - This command installs golang, protoc, git, package-managers, go-tools on the system        
usage :
**trishul install go --goroot $GOROOT --gopath $GOPATH**  - goroot and gopath flags are optional. Default values are OS dependent.
**trishul install protoc** - installation of protoc begins only after installation of golang.
**trishul install git** - installs latest version of git     
**trishul install manager** - installs choco for windows and homebrew for darwin.  
**trishul install tools** - installs gotools, mockery, goconvey.        

**Create** - This command generates the boilerplate code for a micro-service.   
usage : 
**trishul create -i proto-folder  -o output-folder**     
**trishul create --input proto-folder  --output output-folder**     
The flags i and o are required.         
There are two other flags --pb_only(-p) & --force (-f).
--p or --pb_only flag is used to generate pb files only.
-f or --force is used to overwrite the existing files.
This command generates boilerplate code, go mod, generates the pb.go file of protos and git initialized repository. 

**Convey** - This command generates the boilerplate code for testing using convey.  
usage : 
**trishul convey -i input-folder  -o output-folder**     
**trishul convey --input input-folder  --output output-folder**     
The flags i and o are required.         
This command generates boilerplate code using specs in .txt files present in input folder. 

**Analyze** - This command analyzes packages provided as args.  
usage : 
**trishul analyze [packages ...]**      
This command analyzes and formats each and every package.
It uses gofmt for formatting, govet and revive for linting, and runs the testcases to show the coverage.
It also gets the required packages.

<h2> Installation of Trishul </h2>
For linux download and run trishul-install-linux.sh
For darwin(MacOS) download and run trishul-install-darwin.sh
For windows download and run trishul-install-windows.bat